/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamespoly;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;

/**
 *
 * @author mstep_000
 */
public class PlayMenu extends JFrame{
    private JButton utworz;
    private JButton wczytaj;
    private JButton dolacz;
    private JButton wroc;
    public static final int height = 525;
    public static final int width = 455;
    PlayMenu(){
        super("Graj!");
        //pobieranie szerokosci/wysokosci ekranu użytkownika
        int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        // Ustawienia ramki
        setBounds(screenWidth/2 - width/2, (screenHeight/2-height/2), width, height);
        setSize(width,height);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        //baza
        JLayeredPane lp = getLayeredPane();
        //tlo
        Icon obraz = new ImageIcon(getClass().getResource("Okna/menu_tlo.png"));
        JLabel tlo = new JLabel(obraz);
        tlo.setBounds(0, 0, 450,500);
        //utworz
        Icon b = new ImageIcon(getClass().getResource("Buttons/utworz.png"));
        Icon b1 = new ImageIcon(getClass().getResource("Buttons/utworz2.png"));
        utworz = new JButton(b);
        utworz.setRolloverIcon(b1);
        utworz.setBounds(60, 190, 230, 45);
        utworz.setContentAreaFilled(false);
        utworz.setBorderPainted(false);
        //dolacz
        b = new ImageIcon(getClass().getResource("Buttons/dolacz.png"));
        b1 = new ImageIcon(getClass().getResource("Buttons/dolacz2.png"));
        dolacz = new JButton(b);
        dolacz.setRolloverIcon(b1);
        dolacz.setBounds(60, 240, 230, 45);
        dolacz.setContentAreaFilled(false);
        dolacz.setBorderPainted(false);
        //wczytaj
        b = new ImageIcon(getClass().getResource("Buttons/wczytaj_sesje.png"));
        b1 = new ImageIcon(getClass().getResource("Buttons/wczytaj_sesje2.png"));
        wczytaj = new JButton(b);
        wczytaj.setRolloverIcon(b1);
        wczytaj.setBounds(60, 290, 230, 45);
        wczytaj.setContentAreaFilled(false);
        wczytaj.setBorderPainted(false);
        //wroc
        b = new ImageIcon(getClass().getResource("Buttons/wroc.png"));
        b1 = new ImageIcon(getClass().getResource("Buttons/wroc2.png"));
        wroc = new JButton(b);
        wroc.setRolloverIcon(b1);
        wroc.setBounds(60, 370, 230, 45);
        wroc.setContentAreaFilled(false);
        wroc.setBorderPainted(false);
        //dodawanie komponentow
        lp.add(utworz);
        lp.add(dolacz);
        //lp.add(wczytaj);
        lp.add(wroc);
        lp.add(tlo);
        
        HandlerClass handler = new HandlerClass();
        utworz.addActionListener(handler);
        dolacz.addActionListener(handler);
        wczytaj.addActionListener(handler);
        wroc.addActionListener(handler);
  }
  private class HandlerClass implements ActionListener{
      @Override
      public void actionPerformed(ActionEvent event){
          String ip, name;
          if (event.getSource()==utworz){
              dispose();
              String ile = JOptionPane.showInputDialog("Ile graczy(2-8): ");
              int q=Integer.parseInt(ile);
              Thread gra=new Thread(new Game(q));
              gra.start();
              name = JOptionPane.showInputDialog("Podaj imie");
              Thread serverClient;
              try {
                  serverClient = new Thread(new ClientHandler("127.0.0.1",name));
                  serverClient.start();
              } catch (FileNotFoundException ex) {} catch (IOException ex) {
                  Logger.getLogger(PlayMenu.class.getName()).log(Level.SEVERE, null, ex);
              }
          }
          else if (event.getSource()==dolacz){
                ip = JOptionPane.showInputDialog("Podaj ip");
                //ip="127.0.0.1";
                name = JOptionPane.showInputDialog("Podaj imie");
                //name="cokolwiek";
                dispose();
                Thread gra;
              try {
                  gra = new Thread(new ClientHandler(ip,name));
                  gra.start();
              } catch (FileNotFoundException ex) {} catch (IOException ex) {
                  Logger.getLogger(PlayMenu.class.getName()).log(Level.SEVERE, null, ex);
              }
          }
          else if (event.getSource()==wczytaj)
                JOptionPane.showMessageDialog(null, String.format("Kurczę, bolało nawet bardziej!",event.getActionCommand()));
          else if (event.getSource()==wroc){
              dispose();
              MainMenu menu=new MainMenu();
          }
          
      }
  }

}
