/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamespoly;

import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

/**
 *
 * @author mstep_000
 */
public class FieldCard extends JFrame {
    public static final int height = 300+25;
    public static final int width = 600; 
    public JLayeredPane lp;
    public JLabel txt;
    FieldCard(String q ){
        super("Oho!");
        //pobieranie szerokosci/wysokosci ekranu użytkownika
        int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        // Ustawienia ramki
        setBounds(screenWidth/2 - width/2, (screenHeight/2-height/2), width, height);
        setSize(width,height);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        
        
        //baza
        lp = getLayeredPane();
        //plansza
        Icon obraz = new ImageIcon(getClass().getResource("ButtonsGame/Karta.jpg"));
        JLabel tlo = new JLabel(obraz);
        tlo.setBounds(0, 0, 600,300);
        
        txt=new JLabel(q);
        txt.setBounds(210,150, 300, 20);

        
        lp.add(tlo,10);
        lp.add(txt, 0);
    }
}
