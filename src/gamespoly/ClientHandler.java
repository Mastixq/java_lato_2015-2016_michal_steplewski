/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamespoly;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author mstep_000
 */
public class ClientHandler implements Runnable {
    //<editor-fold defaultstate="collapsed" desc="Deklaracja zmiennych">
    public static final int PORT=49159;
    //ustawienia gracza
    private String ip, name;
    public int position, ktory;
    //ustawienia tablicy graczy
    private Player player[];
    private Pawn pionki=new Pawn();
    private Pawn pionkiKtory=new Pawn();
    private JLabel pionkiActive=new JLabel();
    private int ile=0;
    //ustawienia gry
    HandlerClass handler;
    private Plansza plansza;
    //gra
    private int kostka1;
    private int kostka2;
    private int activePlayer;
    private int playable[];
    //okna
    private Pop_up kupno;
    private GameWindow game;
    private OfferMenu oferta;
    private Yes_No potwierdz;
    
    //info
    private JLabel selfInfo1, selfInfo2, selfInfo3, selfPawn;
    private JLabel currInfo1, currInfo2, currInfo3, currPawn;
    ImageIcon imagePionki[]=new ImageIcon[8];
    
    private JLabel info1, info2, info3;
    //rozwijana lista
    JComboBox fieldCombo;
    //server
    private Socket ss;
    private DataOutputStream out;
    private DataInputStream in;
    private InputStream in_sock;
    private OutputStream out_sock;
    //zmienneEvents
    public int Event=-1;
    public int selfEvent=-1;
    public boolean ifEvent=false;
    private boolean rolling=false;
    private boolean running=true;
    private boolean movement=false;
    private boolean rollable=false;
    private boolean fieldCheck=false;
    public int zmiennaEvent1, zmiennaEvent2, zmiennaEvent3;
    ClientHandler q;
    //pliki
    PrintWriter zapisLog;
//</editor-fold>
    
    ClientHandler(String ip, String name) throws FileNotFoundException, IOException{
        this.ip=ip;
        this.name=name;
        boolean serverTest=true;
        
        try {
            BD bd=new BD();
            
            PreparedStatement prep = bd.conn.prepareStatement("insert into UserLog values (?,?,?);");
 
            prep.setString(2, this.name);
            prep.addBatch();
        
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String s=dateFormat.format(date);
            
            prep.setString(3, s);
            prep.addBatch();
 
            bd.conn.setAutoCommit(false);
            prep.executeBatch();
            bd.conn.setAutoCommit(true);
        
            bd.rs.close();
            bd.conn.close();
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Plansza.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Plansza.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        //selfPawn=pionkiKtory.pawns[ktory];
        while(serverTest){
        try {
            ss=new Socket(ip,PORT);
            in_sock = ss.getInputStream();
            in = new DataInputStream ( in_sock );
            ile= in.readInt();
            out_sock = ss.getOutputStream();
            out = new DataOutputStream ( out_sock );
            
            imagePionki[0]=new ImageIcon(getClass().getResource("Pawns/PawnYellow.png"));
            imagePionki[1]=new ImageIcon(getClass().getResource("Pawns/PawnBlue.png"));
            imagePionki[2]=new ImageIcon(getClass().getResource("Pawns/PawnGreen.png"));
            imagePionki[3]=new ImageIcon(getClass().getResource("Pawns/PawnBrown.png"));
            imagePionki[4]=new ImageIcon(getClass().getResource("Pawns/PawnCeladon.png"));
            imagePionki[5]=new ImageIcon(getClass().getResource("Pawns/PawnPink.png"));
            imagePionki[6]=new ImageIcon(getClass().getResource("Pawns/PawnRed.png"));
            imagePionki[7]=new ImageIcon(getClass().getResource("Pawns/PawnViolet.png"));
            
            if(ile>0){
                ktory=in.readInt();
                activePlayer=in.readInt();
                rollable=in.readBoolean();
                out.writeInt(name.length());
                out.writeChars(name);
                break;
            }
            else 
                this.ip = JOptionPane.showInputDialog("Podaj ip");
            

        }catch (Exception ex) {}
            this.ip = JOptionPane.showInputDialog("Podaj ip");
        }
        
        //inicjalizacja okna
        game=new GameWindow();
        
        //inicjalizacja kostki
        kostka1=0;
        kostka2=0;
        
        //inicjalizacja planszy
        plansza=new Plansza();
        
        playable=new int[ile];
        for (int i=0; i<ile; i++)
            playable[i]=1;
        //inicjalizacja comboBoxa
        fieldCombo=new JComboBox();
        fieldCombo.setBounds(725,285, 380, 20);
        for (int i=0; i<40; i++){
            if(plansza.pola[i].typ==1)
                fieldCombo.addItem(plansza.pola[i].name);
          
        }
        
        ShowFieldInfo();
         game.lp.add(fieldCombo,0);
        //inicjalizacja listenera
        handler = new HandlerClass();
        fieldCombo.addActionListener(handler);
        game.tmp.addActionListener(handler);
        game.tmp1.addActionListener(handler);
        game.buduj.addActionListener(handler);
        game.wrocMenu.addActionListener(handler);
        game.oferta.addActionListener(handler);
        game.zastaw.addActionListener(handler);
        game.wykup.addActionListener(handler);
        game.zapisz.addActionListener(handler);
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String s=dateFormat.format(date);
        zapisLog = new PrintWriter("logs.txt");
        zapisLog.println("Rozgrywka odbyła sie: "+s);
        zapisLog.close();
    }
    
    
    @Override
    public void run() {
        
        try {
            q=this;
        
        //inicjalizacja graczy
        player=new Player[ile];
        String s=readFile("logs.txt");
        zapisLog = new PrintWriter("logs.txt");
        zapisLog.print(s);
        zapisLog.println("Gracze: ");
        for(int i=0; i<ile; i++){

            player[i]=new Player();
            player[i].pawn=pionki.pawns[i];
            player[i].pawn.setBounds(plansza.pola[0].point[i]);
            plansza.pola[0].ileNaPolu++;
            int k=in.readInt(); //ilosc znakow w slowie 
            player[i].nazwa="";
            for(int j=0; j<k;j++){
            char q=in.readChar();
            player[i].nazwa+=Character.toString(q);
            System.out.println(player[i].nazwa);
            }
            game.lp.add(player[i].pawn, ile-1-i); 
            zapisLog.println(player[i].nazwa);
         }  
        zapisLog.close();
        showInfo();
        while(running){
            updateInfo();
            Thread.sleep(1000/60);
            
            if(!player[ktory].hajsSieZgadza()){
                String p=readFile("logs.txt");
                zapisLog = new PrintWriter("logs.txt");
                zapisLog.print(p);
                zapisLog.println("Przegrałes!");
                zapisLog.close();
                FieldCard q=new FieldCard("Przegrales :(");
                
                ifEvent=true;
                selfEvent=7;
            }


            
            if(ifEvent){
                out.writeInt(selfEvent);
                ifEvent=false;
                if(selfEvent==3){
                    out.writeInt(zmiennaEvent1);
                }
                if(selfEvent==4){
                    out.writeInt(zmiennaEvent1);//od kogo
                    out.writeInt(zmiennaEvent2);//co
                    out.writeInt(zmiennaEvent3);//za ile 
                    out.writeInt(ktory);//kto chce kupic
                }
                if (selfEvent==5){
                    out.writeInt(zmiennaEvent1);//index pola
                    out.writeInt(zmiennaEvent2);//za ile
                    out.writeInt(zmiennaEvent3);//komu 
                }
            }
            else 
                out.writeInt(-1);
            
            
            for(int i=0; i<ile; i++){
                if(playable[i]!=1)
                    continue;
                Event=in.readInt();
                //System.out.println("Dostalem event: "+Event+" od: "+i);
                if (Event>=0){
                    String p=readFile("logs.txt");
                    zapisLog = new PrintWriter("logs.txt");
                    zapisLog.print(p);
                    zapisLog.println("Dostalem event: "+Event+" od: "+i);
                    zapisLog.close();
                    switch(Event){
                        case 0:
                            activePlayer=i;
                            movement();
                            break;  
                        case 1:
                            activePlayer=in.readInt();
                            if (ktory==activePlayer)
                                rollable=true;
                            break;
                        case 2: //kupno
                            int buyPlayer=in.readInt();
                            buyField(buyPlayer,player[buyPlayer].index,plansza.pola[player[buyPlayer].index].value);
                            System.out.println("kupiono: " + plansza.pola[player[buyPlayer].index].name + " przez: " + player[buyPlayer].nazwa + "za: " + plansza.pola[player[buyPlayer].index].value );
                            plansza.pola[player[buyPlayer].index].isFree=false;
                            break;
                        case 3: //moneyChange
                            zmiennaEvent1=in.readInt();//ktory gracz
                            zmiennaEvent2=in.readInt();//delta
                            updateMoney(zmiennaEvent1,-zmiennaEvent2);
                            break;
                        case 4://oferta
                            zmiennaEvent1=in.readInt();//index pola
                            zmiennaEvent2=in.readInt();//za ile
                            zmiennaEvent3=in.readInt();//komu
                            s=plansza.pola[zmiennaEvent1].name;
                            System.out.println("Ktos chce kupic pole!");
                            potwierdz=new Yes_No(s,zmiennaEvent2);
                            potwierdz.accept.addActionListener(handler);
                            potwierdz.decline.addActionListener(handler);
                            break;
                        case 5:
                            zmiennaEvent1=in.readInt();//index pola
                            zmiennaEvent2=in.readInt();//za ile
                            zmiennaEvent3=in.readInt();//komu
                            int tmp = plansza.pola[zmiennaEvent1].owner;
                            changeField(zmiennaEvent3,zmiennaEvent1);
                            PayPlayer(tmp, zmiennaEvent3, zmiennaEvent2);
                            break;
                        case 6:
                            zmiennaEvent1=in.readInt();
                            plansza.pola[player[zmiennaEvent1].index].ileNaPolu--;
                            player[zmiennaEvent1].index=10;
                            player[zmiennaEvent1].pawn.setBounds(plansza.pola[10].point[plansza.pola[player[activePlayer].index].ileNaPolu]);
                            plansza.pola[player[zmiennaEvent1].index].ileNaPolu++;
                            game.lp.setLayer(player[zmiennaEvent1].pawn, plansza.pola[player[activePlayer].index].ileNaPolu);
                            break;
                        case 7: //gracz przegrywa 
                            playable[i]=0;
                            ifEvent=true;
                            selfEvent=1;
                            break;
                            
                    }
                }
            }
            

            //<editor-fold defaultstate="collapsed" desc="Animacja kostki">
            if (rolling){
                System.out.println("Rolling...");
            for (int i=0; i<15; i++){
                ImageIcon b=game.roll();
                while (game.kostka1.getIcon()==b)
                    b=game.roll();
                game.kostka1.setIcon(b);
                b=game.roll();
                while (game.kostka2.getIcon()==b)
                    b=game.roll();
                game.kostka2.setIcon(b);
                try {
                    Thread.sleep(140);
                } catch (InterruptedException ex) {}
                rollable=false;
            }
            game.kostka1.setIcon(game.kostki[kostka1]);
            game.kostka2.setIcon(game.kostki[kostka2]);
            rolling=false;
            movement=true;
    }
//</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Movement">
            if (movement){
                for (int i=0; i<kostka1+kostka2+2; i++){ //
                    plansza.pola[player[activePlayer].index].ileNaPolu--;
                    player[activePlayer].index++;
                    if(player[activePlayer].index==40){
                        player[activePlayer].index=0;
                        player[activePlayer].money+=400;
                        
                    }
                    player[activePlayer].pawn.setBounds(plansza.pola[player[activePlayer].index].point[plansza.pola[player[activePlayer].index].ileNaPolu]);
                    plansza.pola[player[activePlayer].index].ileNaPolu++;
                    game.lp.setLayer(player[activePlayer].pawn, plansza.pola[player[activePlayer].index].ileNaPolu);
                    try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {}
                }
                movement=false;
                fieldCheck=true;
                zapisLog.println("Gracz: "+activePlayer+"Wyladowal na polu: "+ plansza.pola[player[activePlayer].index].name);
            }
                //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Sprawdzenie pola">
            if (fieldCheck){
                if(ktory==activePlayer && plansza.pola[player[activePlayer].index].typ==7){
                    ifEvent=true;
                    selfEvent=3;
                    zmiennaEvent1=-200;
                    FieldCard q=new FieldCard("Kupujesz bitcoiny tylko za 200 cebulionow!\n Niestety sa bezutyczne...");
                    
                }
                else if(ktory==activePlayer && plansza.pola[player[activePlayer].index].typ==2){
                    ifEvent=true;
                    selfEvent=3;
                    zmiennaEvent1=400;
                    FieldCard q=new FieldCard("Zaskakujacy bug, otrzymujesz 400 cebulionow!");
                     zapisLog.println("Zaskakujacy bug, otrzymujesz 400 cebulionow!");
                }
                else if(ktory==activePlayer && plansza.pola[player[activePlayer].index].typ==3){
                    ifEvent=true;
                    selfEvent=3;
                    zmiennaEvent1=-100;
                    FieldCard q=new FieldCard("Maly update, tracisz 100 cebulionow przez restart");
                    zapisLog.println("Maly update, tracisz 100 cebulionow przez restart");
                }
                else if(ktory==activePlayer && plansza.pola[player[activePlayer].index].typ==4){
                    ifEvent=true;
                    selfEvent=6;
                    FieldCard q=new FieldCard("Trafiasz do wiezienie, dlugo tu nie zostaniesz...");
                    zapisLog.println("Trafiasz do wiezienie, dlugo tu nie zostaniesz...");
                }
                else if (ktory==activePlayer && plansza.pola[player[activePlayer].index].isFree && plansza.pola[player[activePlayer].index].typ==1 ){
                    kupno=new Pop_up();
                    kupno.accept.addActionListener(handler);
                    kupno.decline.addActionListener(handler);
                }
                else{
                    if (plansza.pola[ktory].owner==ktory || plansza.pola[player[ktory].index].owner==-1 ){
                        fieldCheck=false;
                        continue;                        
                    }  

                    else{
                        try{
                        System.out.println("Zaplacono: "+plansza.pola[player[activePlayer].index].owner+" ile: "+ plansza.pola[player[activePlayer].index].fee);
                        PayPlayer(plansza.pola[player[activePlayer].index].owner, activePlayer, plansza.pola[player[activePlayer].index].fee);
                        }catch(Exception ex){fieldCheck=false;}
                    }
                }
            }
            fieldCheck=false;


        }
        } catch (Exception ex) {
        fieldCheck=false;
        }
        //</editor-fold>
    }
    
    private class HandlerClass implements ActionListener {
      @Override
      public void actionPerformed(ActionEvent event){
          //losuj
          if (event.getSource()==game.tmp){
              if(rollable){
                ifEvent=true;
                selfEvent=0;
              }
              else 
                  JOptionPane.showMessageDialog(null, String.format("Nie mozesz losowac!",event.getActionCommand()));
                  
          }
          //zakoncz ture
          else if (event.getSource()==game.tmp1){
              if(ktory==activePlayer && !rollable){
                ifEvent=true;
                selfEvent=1;
              }
              else
                   JOptionPane.showMessageDialog(null, String.format("Active player: "+activePlayer+" rollable: "+rollable,event.getActionCommand()));
                  
          }
          //buduj
          else if (event.getSource()==game.buduj){
                ifEvent=true;
                selfEvent=3;
                System.out.println("Chce budowac!");       
          } 
          //zastaw
          else if (event.getSource()==game.zastaw){
                ifEvent=true;
                selfEvent=4;
                System.out.println("Chce zastawic");       
          }  
          //oferta
          else if (event.getSource()==game.oferta){
              if(ktory==activePlayer){
                    oferta=new OfferMenu(plansza, player,q);
              }
          }  
          //zapisz
          else if (event.getSource()==game.wrocMenu){
                running=false;
                game.dispose();
                MainMenu menu=new MainMenu();          
          }
          //fieldcombox
            else if (event.getSource()==fieldCombo){
                int decisive=0;
                String check=fieldCombo.getSelectedItem().toString();
                for (int i=0; i<40; i++){
                   if(check.equals(plansza.pola[i].name))
                       decisive=i;
                }
                System.out.println("Jestes na polu: "+plansza.pola[decisive].name);
                UpdateFieldInfo(decisive);
          }
            
            else if (event.getSource()==kupno.accept){
              ifEvent=true;
              selfEvent=2;
              kupno.dispose();
            }
            else if (event.getSource()==kupno.decline){
              kupno.dispose();
            }
            else if (event.getSource()==potwierdz.accept){
              ifEvent=true;
              selfEvent=5;
              potwierdz.dispose();
            }
            else if (event.getSource()==potwierdz.decline){
              potwierdz.dispose();
            }          
            
          
    
        }
  }
    void movement() throws IOException{
        rolling = true;
        kostka1=in.readInt();
        System.out.println("Odebralem kostka1= "+kostka1);
        kostka2=in.readInt();
        System.out.println("Odebralem kostka2= "+kostka2);
        String p=readFile("logs.txt");
        zapisLog = new PrintWriter("logs.txt");
        zapisLog.print(p);
        zapisLog.println("Kostka1: "+kostka1+" Kostka2"+kostka2);
        zapisLog.close();
   } 
    void showInfo(){
        //info 
        JLabel you=new JLabel("Twoje statystyki: ");
        you.setBounds(725, 0, 130, 45);
        game.lp.add(you,0);
        
        //cebulion: 
        ImageIcon b = new ImageIcon(getClass().getResource("ButtonsGame/waluta.png"));
        JLabel cebulion = new JLabel(b);
        cebulion.setBounds(870, 75, 25, 25);
        game.lp.add(cebulion,0);
        
        selfInfo1=new JLabel("Pozycja:");
        selfInfo1.setBounds(725, 15, 130, 45);
        game.lp.add(selfInfo1,0);
        
        String s=plansza.pola[player[ktory].index].name;
        selfInfo2=new JLabel("");
        selfInfo2.setBounds(725, 45, 130, 15);
        setFont(selfInfo2);
        game.lp.add(selfInfo2,0);
        
        s=""+player[ktory].money;
        selfInfo3=new JLabel("Srodki: "+s);
        selfInfo3.setBounds(725, 65, 130, 45);
        setFont(selfInfo3);
        game.lp.add(selfInfo3,0);
        
        //Ustawienia pionka
        pionkiKtory.pawns[ktory].setBounds(815, 15, 130, 45);
        game.lp.add(pionkiKtory.pawns[ktory],0);
        
        
        JLabel you2=new JLabel("Aktywne statystyki: ");
        you2.setBounds(925, 0, 130, 45);
        game.lp.add(you2,0);
        
        currInfo1=new JLabel("Pozycja:");
        currInfo1.setBounds(925, 15, 130, 45);
        game.lp.add(currInfo1,0);
        
        s=plansza.pola[player[ktory].index].name;
        currInfo2=new JLabel(s);
        currInfo2.setBounds(925, 45, 130, 15);
        setFont(currInfo2);
        game.lp.add(currInfo2,0);
        
        s=""+player[activePlayer].money;
        currInfo3=new JLabel("Srodki: "+s);
        currInfo3.setBounds(925, 65, 130, 45);
        setFont(currInfo3);
        game.lp.add(currInfo3,0);
        
        
        //cebulion2
        JLabel cebulion2 = new JLabel(b);
        cebulion2.setBounds(1070, 75, 25, 25);
        game.lp.add(cebulion2,0);
        //Ustawienia pionka
        
        pionkiActive = new JLabel(imagePionki[activePlayer]);        
        pionkiActive.setBounds(1015, 15, 130, 45);
        game.lp.add(pionkiActive,0);
        
        
    }
    void updateInfo(){
        String s=plansza.pola[player[ktory].index].name;
        selfInfo2.setText(s);
        s="Srodki: "+player[ktory].money;
        selfInfo3.setText(s);
        
        s=plansza.pola[player[activePlayer].index].name;
        currInfo2.setText(s);
        s="Srodki:"+player[activePlayer].money;
        currInfo3.setText(s);
        
        pionkiActive.setIcon(imagePionki[activePlayer]);
    }
    void updateMoney(int index, int delta){
        player[index].money-=delta;
    }
    void changeField(int index, int indexP){
        plansza.pola[indexP].owner=index;
    }
    void buyFromPlayer(int indexB, int indexS, int indexP, int delta){
        changeField(indexB,indexP);
        updateMoney(indexB, -delta);
        updateMoney(indexS, delta);
    }
    void buyField(int index, int indexP, int delta){
        changeField(index,indexP);
        plansza.pola[indexP].isFree=false;
        updateMoney(index, delta);
    }
    
    //gracz B placy graczowi A
    void PayPlayer(int indexA, int indexB, int delta){
        player[indexA].money+=delta;
        player[indexB].money-=delta;
    }
    void ShowFieldInfo(){
        //info 
        info1=new JLabel(plansza.pola[0].name);
        info1.setBounds(725,305, 380, 20);
        game.lp.add(info1,0);
        
        info2=new JLabel("Wlasciciel: Brak");
        info2.setBounds(725,320, 380, 20);
        game.lp.add(info2,0);  
        
        info3=new JLabel("Oplata: +400");
        info3.setBounds(725,335, 380, 20);
        game.lp.add(info3,0);  
    }
    void UpdateFieldInfo(int indexP){
        info1.setText(plansza.pola[indexP].name);
        int q=plansza.pola[indexP].owner;
        if(q>=0)
            info2.setText("Wlasciciel: "+player[q].nazwa);
        else 
            info2.setText("Wlasciciel: brak!");
        info3.setText("Oplata: "+plansza.pola[indexP].fee);
    }
    void setFont(JLabel label){
        Font labelFont = label.getFont();
        String labelText = label.getText();

        int stringWidth = label.getFontMetrics(labelFont).stringWidth(labelText);
        int componentWidth = label.getWidth();

        // Find out how much the font can grow in width.
        double widthRatio = (double)componentWidth / (double)stringWidth;

        int newFontSize = (int)(labelFont.getSize() * widthRatio);
        int componentHeight = label.getHeight();

        // Pick a new font size so it will not be larger than the height of label.
        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        // Set the label's font size to the newly determined size.
        label.setFont(new Font(labelFont.getName(), Font.PLAIN, fontSizeToUse));
    }
    String readFile(String s) throws FileNotFoundException, IOException{
        FileReader fileReader = new FileReader(s);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String text="";
        try {
            String textLine = bufferedReader.readLine();
            do {
            text+=textLine;
            text+="\r\n";
            System.out.println(textLine);
            textLine = bufferedReader.readLine();
        } while (textLine != null);
        } finally {
        bufferedReader.close();
        return text;
        }
    }
}
