/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamespoly;


import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

/**
 *
 * @author mstep_000
 */
public class GameWindow extends JFrame {
    public JButton tmp;
    public JButton tmp1;
    public JButton buduj;
    public JButton oferta;
    public JButton wykup;
    public JButton zastaw;
    public JButton zapisz;
    public JButton wrocMenu;
    public JLabel kostka1;
    public JLabel kostka2;
    public JLayeredPane lp;
    public ImageIcon kostki[];
    public JLabel player[];
    public JLabel pola[]=new JLabel[40];
    public static final int height = 720+25;
    public static final int width = 720+5+400;
    GameWindow(){
        super("Michał Stęplewski-Gamespoly!");
        //pobieranie szerokosci/wysokosci ekranu użytkownika
        int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        // Ustawienia ramki
        setBounds(screenWidth/2 - width/2, (screenHeight/2-height/2), width, height);
        setSize(width,height);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
      
        //baza
        lp = getLayeredPane();
        //plansza
        Icon obraz = new ImageIcon(getClass().getResource("Okna/PlanszaFinal.png"));
        JLabel tlo = new JLabel(obraz);
        tlo.setBounds(0, 0, 720,720);
        //menu_gry
        obraz = new ImageIcon(getClass().getResource("Okna/gamewindow_tlo.png"));
        JLabel menu = new JLabel(obraz);
        menu.setBounds(560, 0, 720, 720);
        //ustawienie tooltipow
        obraz = new ImageIcon(getClass().getResource("Okna/dot.png"));
        pola[0]=new JLabel(obraz);
        pola[0].setBounds(600, 600, 120, 120);
        pola[0].setToolTipText("Pole start, otrzymujesz 400$ po przejsciu!");
        lp.add(pola[0], 0);
        
        //kostki
        obraz = new ImageIcon(getClass().getResource("Kostka/cztery.png"));
        kostka1 = new JLabel(obraz);
        kostka1.setBounds(765, 200, 50, 50);
        
        
        obraz = new ImageIcon(getClass().getResource("Kostka/dwa.png"));
        kostka2 = new JLabel(obraz);
        kostka2.setBounds(825, 200, 50, 50);
        //Kostki_obraz
        kostki=new ImageIcon[6];
        kostki[0]=new ImageIcon(getClass().getResource("Kostka/jeden.png"));
        kostki[1]=new ImageIcon(getClass().getResource("Kostka/dwa.png"));
        kostki[2]=new ImageIcon(getClass().getResource("Kostka/trzy.png"));
        kostki[3]=new ImageIcon(getClass().getResource("Kostka/cztery.png"));
        kostki[4]=new ImageIcon(getClass().getResource("Kostka/piec.png"));
        kostki[5]=new ImageIcon(getClass().getResource("Kostka/szesc.png"));
        //losuj
        Icon b = new ImageIcon(getClass().getResource("ButtonsGame/losuj.png"));
        Icon b1 = new ImageIcon(getClass().getResource("ButtonsGame/losuj2.png"));
        tmp = new JButton(b);
        tmp.setRolloverIcon(b1);
        tmp.setBounds(745, 120, 148, 63);
        tmp.setContentAreaFilled(false);
        tmp.setBorderPainted(false);
        //tmp1
        b = new ImageIcon(getClass().getResource("ButtonsGame/zakoncz.png"));
        b1 = new ImageIcon(getClass().getResource("ButtonsGame/zakoncz2.png"));
        tmp1 = new JButton(b);
        tmp1.setRolloverIcon(b1);
        tmp1.setBounds(945, 120, 150, 65);
        tmp1.setContentAreaFilled(false);
        tmp1.setBorderPainted(false);        
        //buduj
        b = new ImageIcon(getClass().getResource("ButtonsGame/buduj.png"));
        b1 = new ImageIcon(getClass().getResource("ButtonsGame/buduj2.png"));
        buduj = new JButton(b);
        buduj.setRolloverIcon(b1);
        buduj.setBounds(945, 120, 150, 65);
        buduj.setContentAreaFilled(false);
        buduj.setBorderPainted(false); 
        
        //oferta
        b = new ImageIcon(getClass().getResource("ButtonsGame/oferta.png"));
        b1 = new ImageIcon(getClass().getResource("ButtonsGame/oferta2.png"));
        oferta = new JButton(b);
        oferta.setRolloverIcon(b1);
        oferta.setBounds(945, 200, 150, 65);
        oferta.setContentAreaFilled(false);
        oferta.setBorderPainted(false); 
        
        //zastaw
        b = new ImageIcon(getClass().getResource("ButtonsGame/zastaw.png"));
        b1 = new ImageIcon(getClass().getResource("ButtonsGame/zastaw2.png"));
        zastaw = new JButton(b);
        zastaw.setRolloverIcon(b1);
        zastaw.setBounds(945, 402, 150, 65);
        zastaw.setContentAreaFilled(false);
        zastaw.setBorderPainted(false); 
        
        //wykup
        b = new ImageIcon(getClass().getResource("ButtonsGame/wykup.png"));
        b1 = new ImageIcon(getClass().getResource("ButtonsGame/wykup2.png"));
        wykup = new JButton(b);
        wykup.setRolloverIcon(b1);
        wykup.setBounds(745, 485, 150, 65);
        wykup.setContentAreaFilled(false);
        wykup.setBorderPainted(false); 
        
        //zapisz
        b = new ImageIcon(getClass().getResource("ButtonsGame/zapisz.png"));
        b1 = new ImageIcon(getClass().getResource("ButtonsGame/zapisz2.png"));
        zapisz = new JButton(b);
        zapisz.setRolloverIcon(b1);
        zapisz.setBounds(945, 485, 150, 65);
        zapisz.setContentAreaFilled(false);
        zapisz.setBorderPainted(false);
        
        
        //wrocMenu
        b = new ImageIcon(getClass().getResource("ButtonsGame/wyjdz.png"));
        b1 = new ImageIcon(getClass().getResource("ButtonsGame/wyjdz2.png"));
        wrocMenu = new JButton(b);
        wrocMenu.setRolloverIcon(b1);
        wrocMenu.setBounds(830,640, 150, 65);
        wrocMenu.setContentAreaFilled(false);
        wrocMenu.setBorderPainted(false);  
       
        
        lp.add(tmp);
        lp.add(tmp1);
        //lp.add(buduj);
        //lp.add(wykup);
        lp.add(oferta);
        //lp.add(zastaw);
        //lp.add(zapisz);
        lp.add(menu, 10);
        lp.add(wrocMenu,0);
        lp.add(tlo);
        lp.add(kostka1,0);
        lp.add(kostka2,0);
    
  }
  
  public ImageIcon roll() {
      Random g=new Random();
      return (kostki[g.nextInt(6)]);  
  }
  
  
}