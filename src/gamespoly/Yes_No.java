/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamespoly;

import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

/**
 *
 * @author mstep_000
 */
public class Yes_No extends JFrame {
        public JButton accept;
    public JButton decline;
    public JLayeredPane lp;
    public static final int height = 100+25;
    public static final int width = 400; 
    Yes_No(String q, int w){
        super("Czy chcesz sprzedac "+q+" za: "+w+"?");
        //pobieranie szerokosci/wysokosci ekranu użytkownika
        int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        // Ustawienia ramki
        setBounds(screenWidth/2 - width/2, (screenHeight/2-height/2), width, height);
        setSize(width,height);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        
        
        //baza
        lp = getLayeredPane();
        //plansza
        Icon obraz = new ImageIcon(getClass().getResource("Okna/gamewindow_tlo.png"));
        JLabel tlo = new JLabel(obraz);
        tlo.setBounds(0, 0, 400,130);
        
        
        Icon b = new ImageIcon(getClass().getResource("ButtonsGame/przyjmij.png"));
        Icon b1 = new ImageIcon(getClass().getResource("ButtonsGame/przyjmij2.png"));
        accept = new JButton(b);
        accept.setRolloverIcon(b1);
        accept.setBounds(40, 20, 150, 45);
        accept.setContentAreaFilled(false);
        accept.setBorderPainted(false);
        //tmp1
        b = new ImageIcon(getClass().getResource("ButtonsGame/odrzuc.png"));
        b1 = new ImageIcon(getClass().getResource("ButtonsGame/odrzuc2.png"));
        decline = new JButton(b);
        decline.setRolloverIcon(b1);
        decline.setBounds(220, 20, 150, 45);
        decline.setContentAreaFilled(false);
        decline.setBorderPainted(false);     
        
        lp.add(tlo,10);
        lp.add(accept, 0);
        lp.add(decline,0);
    }
}
