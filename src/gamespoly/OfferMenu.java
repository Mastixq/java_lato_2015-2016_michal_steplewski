/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamespoly;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author mstep_000
 */
public class OfferMenu extends JFrame {
    public JButton accept,decline;
    public JTextField delta;
    public JComboBox playerCombo, fieldCombo;
    public JLayeredPane lp;
    public int indexP;
    public Player players[];
    public Plansza a;
    public int w;
    private HandlerClass handler;
    public ClientHandler CL;
    public static final int height = 125+25;
    public static final int width = 400; 
    OfferMenu(Plansza plansza, Player player[], ClientHandler CL){
        super("Co chcesz zrobic?");
        a=plansza;
        this.CL=CL;
        //pobieranie szerokosci/wysokosci ekranu użytkownika
        int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        // Ustawienia ramki
        setBounds(screenWidth/2 - width/2, (screenHeight/2-height/2), width, height);
        setSize(width,height);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        
        
        //baza
        lp = getLayeredPane();
        //plansza
        Icon obraz = new ImageIcon(getClass().getResource("Okna/gamewindow_tlo.png"));
        JLabel tlo = new JLabel(obraz);
        tlo.setBounds(0, 0, 400,130);
        
        //accept
        Icon b = new ImageIcon(getClass().getResource("ButtonsGame/oferta.png"));
        Icon b1 = new ImageIcon(getClass().getResource("ButtonsGame/oferta2.png"));
        accept = new JButton(b);
        accept.setRolloverIcon(b1);
        accept.setBounds(40, 70, 150, 45);
        accept.setContentAreaFilled(false);
        accept.setBorderPainted(false);

        //ile
        delta=new JTextField("Za ile?");
        delta.setBounds(100, 50, 180, 20);
        //decline
        b = new ImageIcon(getClass().getResource("ButtonsGame/odrzuc.png"));
        b1 = new ImageIcon(getClass().getResource("ButtonsGame/odrzuc2.png"));
        decline = new JButton(b);
        decline.setRolloverIcon(b1);
        decline.setBounds(220, 70, 150, 45);
        decline.setContentAreaFilled(false);
        decline.setBorderPainted(false);  
        
        //inicjalizacja comboBoxa
        playerCombo=new JComboBox();
        playerCombo.setBounds(100, 0, 180, 20);
        w=0;
        for (Player x: player){
                w++;
                if (CL.ktory==w-1)
                    continue;
                playerCombo.addItem(x.nazwa);
                
        }
        players=new Player[w];
        for (int i=0; i<w; i++){
                players[i]=player[i];
        }
        
        fieldCombo=new JComboBox();
        fieldCombo.setBounds(100, 25, 180, 20);
        
        lp.add(fieldCombo,0);
        lp.add(delta,0);
        lp.add(tlo,10);
        lp.add(accept, 0);
        lp.add(decline, 0);
        lp.add(playerCombo,0);
        
        //inicjalizacja listenera
        handler = new HandlerClass();
        fieldCombo.addActionListener(handler);
        playerCombo.addActionListener(handler);
        decline.addActionListener(handler);
        accept.addActionListener(handler);
    }
    private class HandlerClass implements ActionListener {
      @Override
      public void actionPerformed(ActionEvent event){
          if (event.getSource()==playerCombo){
                fieldCombo.removeAllItems();
                int decisive=0;
                String check=playerCombo.getSelectedItem().toString();
                for (int i=0; i<w; i++){
                   if(check.equals(players[i].nazwa))
                       decisive=i;
                }
                for (int i=0; i<40; i++){
                    if (a.pola[i].owner==decisive)
                        fieldCombo.addItem(a.pola[i].name);
                }
                CL.zmiennaEvent1=decisive;
                playerCombo.removeActionListener(handler);
        
            }
          else if(event.getSource()==fieldCombo){
                int decisive=0;
                String check=fieldCombo.getSelectedItem().toString();
                for (int i=0; i<40; i++){
                   if(check.equals(a.pola[i].name))
                       decisive=i;
                }
                CL.zmiennaEvent2=decisive;
          }
          else if(event.getSource()==accept){
              CL.ifEvent=true;
              CL.selfEvent=4;
              String text = delta.getText();
              CL.zmiennaEvent3=Integer.parseInt(text);
              System.out.println("Chce kupic od : "+CL.zmiennaEvent1+"pole: "+CL.zmiennaEvent2+"za: "+CL.zmiennaEvent3);
              dispose();
          }
          else if(event.getSource()==decline){
              CL.UpdateFieldInfo(7);
              dispose();
          }
        }
    }

}
