/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamespoly;

import java.awt.Rectangle;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mstep_000
 */
public class Plansza {
    public Pole pola[]=new Pole[40];
    
    
    Plansza(){
        //start
        int licznik=0;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(630, 630, 30, 45);
        pola[licznik].point[1]=new Rectangle(630, 630+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(630, 630+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(630, 630+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(630+30, 630, 30, 45);
        pola[licznik].point[5]=new Rectangle(630+30, 630+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(630+30, 630+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(630+30, 630+45, 30, 45);
        
        //Ubisoft, Assassins Creed
        int width=570;
        int height=630;
        licznik=1;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Community chest 1
        width=510;
        height=630;
        licznik=2;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Ubisoft, Farcry
        width=450+3;
        height=630;
        licznik=3;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
       //Podatek
        width=390+3;
        height=630;
        licznik=4;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Pociongi1
        width=330+3;
        height=630;
        licznik=5;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //EA: Fifa
        width=270+5;
        height=630;
        licznik=6;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Szansa2
        width=210+5;
        height=630;
        licznik=7;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //EA:NFS
        width=150+5;
        height=630;
        licznik=8;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //EA:BF
        width=90+5;
        height=630;
        licznik=9;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Odwiedziny
        width=30+5;
        height=630;
        licznik=10;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Sega:Quake
        width=30+5;
        height=570;
        licznik=11;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //PowerPlant
        width=30+5;
        height=510;
        licznik=12;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Sega:DukeNuken
        width=30+5;
        height=450+2;
        licznik=13;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Sega:TotalWar
        width=30+5;
        height=390+2;
        licznik=14;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Rails2
        width=30+5;
        height=330+3;
        licznik=15;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Bethesda:Fallout
        width=30+5;
        height=270+3;
        licznik=16;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Community Chest 2
        width=30+5;
        height=210+3;
        licznik=17;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Bethesda:TheElderrolls
        width=30+5;
        height=150+3;
        licznik=18;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Bethesda:Dishonored
        width=30+5;
        height=90+3;
        licznik=19;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Parking
        width=30+5;
        height=0;
        licznik=20;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Rockstar:RedDeadRedemtion
        width=90+3;
        height=0;
        licznik=21;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
                
        //Szansa 2
        width=150+3;
        height=0;
        licznik=22;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Rockstar:MaxPayne
        width=210+4;
        height=0;
        licznik=23;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Rockstar:GTA
        width=270+4;
        height=0;
        licznik=24;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Rails3
        width=330+4;
        height=0;
        licznik=25;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
                
        //Valve:HalfLife
        width=390+2;
        height=0;
        licznik=26;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Valve:Left4Dead
        width=450+2;
        height=0;
        licznik=27;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Waterworks
        width=510+2;
        height=0;
        licznik=28;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);  
        
                
        //Valve:CounterStrike
        width=570;
        height=0;
        licznik=29;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        //Wiezienie
        width=630;
        height=0;
        licznik=30;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+15, 30, 45);
        pola[licznik].point[2]=new Rectangle(width, height+30, 30, 45);
        pola[licznik].point[3]=new Rectangle(width, height+45, 30, 45);
        pola[licznik].point[4]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width+30, height+15, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+30, height+30, 30, 45);
        pola[licznik].point[7]=new Rectangle(width+30, height+45, 30, 45);
        
        
        //Blizzard:WorldOfWarcraft
        width=650+8;
        height=90+3;
        licznik=31;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Blizzard:Diablo
        width=650+8;
        height=150+3;
        licznik=32;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //CommunityChest 3
        width=650+8;
        height=210+4;
        licznik=33;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Blizzard:Warcraft
        width=650+8;
        height=270+4;
        licznik=34;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Rails4
        width=650+8;
        height=330+4;
        licznik=35;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Szansa 3
        width=650+8;
        height=390+4;
        licznik=36;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //CDProjektRed:Cyberpunk
        width=650+8;
        height=450+3;
        licznik=37;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //Podatek
        width=650+8;
        height=510+2;
        licznik=38;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        //CDProjektRed:Wiedzmin
        width=650+8;
        height=570+2;
        licznik=39;
        pola[licznik]=new Pole();
        pola[licznik].owner=-1;
        pola[licznik].isFree=true;
        pola[licznik].ileNaPolu=0;
        pola[licznik].point[0]=new Rectangle(width, height, 30, 45);
        pola[licznik].point[1]=new Rectangle(width, height+10, 30, 45);
        pola[licznik].point[2]=new Rectangle(width+30, height, 30, 45);
        pola[licznik].point[3]=new Rectangle(width+30, height+10, 30, 45);
        pola[licznik].point[4]=new Rectangle(width-30, height, 30, 45);
        pola[licznik].point[5]=new Rectangle(width-30, height+10, 30, 45);
        pola[licznik].point[6]=new Rectangle(width+15, height+5, 30, 45);
        pola[licznik].point[7]=new Rectangle(width-15, height+5, 30, 45);
        
        
        
        try {
            BD bd=new BD();
            bd.rs = bd.stat.executeQuery("select * from Pola;");
            while (bd.rs.next()) {
                licznik=bd.rs.getInt("ID");
                pola[licznik].name=bd.rs.getString("Name");
                pola[licznik].value=bd.rs.getInt("Value");
                pola[licznik].fee=bd.rs.getInt("Fee");
                pola[licznik].typ=bd.rs.getInt("Type");
            }
            bd.rs.close();
            bd.conn.close();
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Plansza.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Plansza.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public class Pole{
        public int owner;
        public String name;
        public int typ; //1-gra, 2-szansa, 3-szansa2, 4-wiezienie, 5-parking/odwiedziny, 6-start
        public int value;
        public boolean isFree;
        public int fee;
        public int ileNaPolu;
        public Rectangle point[]=new Rectangle[8]; 
    }
}
