package gamespoly;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class MainMenu extends JFrame {
  private JButton start;
  private JButton wczytaj;
  private JButton exit;
  public static final int height = 525;
  public static final int width = 455;
  
  MainMenu(){
      super("Gamespoly!");
      //pobieranie szerokosci/wysokosci ekranu użytkownika
      int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
      int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
      // Ustawienia ramki
      setBounds(screenWidth/2 - width/2, (screenHeight/2-height/2), width, height);
      setSize(width,height);
      setVisible(true);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setResizable(false);
      
      //baza
      JLayeredPane lp = getLayeredPane();

      //tlo
      Icon obraz = new ImageIcon(getClass().getResource("Okna/menu_tlo.png"));
      JLabel tlo = new JLabel(obraz);
      tlo.setBounds(0, 0, 450,500);
      //start
      Icon b = new ImageIcon(getClass().getResource("Buttons/graj.png"));
      Icon b1 = new ImageIcon(getClass().getResource("Buttons/graj2.png"));
      start = new JButton(b);
      start.setRolloverIcon(b1);
      start.setBounds(60, 190, 150, 45);
      start.setContentAreaFilled(false);
      start.setBorderPainted(false);
      //wczytaj
      b = new ImageIcon(getClass().getResource("Buttons/wczytaj.png"));
      b1 = new ImageIcon(getClass().getResource("Buttons/wczytaj2.png"));
      wczytaj = new JButton(b);
      wczytaj.setRolloverIcon(b1);
      wczytaj.setBounds(60, 240, 150, 45);
      wczytaj.setContentAreaFilled(false);
      wczytaj.setBorderPainted(false);
      //wyjscie
      b = new ImageIcon(getClass().getResource("Buttons/wyjscie.png"));
      b1 = new ImageIcon(getClass().getResource("Buttons/wyjscie2.png"));
      exit = new JButton(b);
      exit.setRolloverIcon(b1);
      exit.setBounds(60, 370, 150, 45);
      exit.setContentAreaFilled(false);
      exit.setBorderPainted(false);
      //dodawanie komponentow
      lp.add(start);
      //lp.add(wczytaj);
      lp.add(exit);
      lp.add(tlo);
      
      HandlerClass handler = new HandlerClass();
      start.addActionListener(handler);
      wczytaj.addActionListener(handler);
      exit.addActionListener(handler);
  }
  private class HandlerClass implements ActionListener{
      @Override
      public void actionPerformed(ActionEvent event){
          //setVisible(false);
          JButton test;
          if (event.getSource()==start){
              dispose();
              PlayMenu menu=new PlayMenu();
          }
          else if (event.getSource()==wczytaj)
                JOptionPane.showMessageDialog(null, String.format("Kurczę, bolało nawet bardziej!",event.getActionCommand()));
          else if (event.getSource()==exit){
                System.exit(0);
          }
          
      }
  }
   
}