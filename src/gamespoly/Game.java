/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamespoly;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;

/**
 *
 * @author mstep_000
 */
public class Game implements Runnable{
    

    
    //server 
    ServerSocket sc;
    private int ileGraczy;
    public static final int PORT=49159;
    //klienci
    Socket client[];
    private String nicks[];
    private boolean running=true;
    private boolean changePlayer=true;
    private int kostka1;
    private int kostka2;
    private int event[];
    private int playable[];
    private int activePlayer=0;
    private int event1, event2, event3, event4;
    private DataOutputStream out;
    private OutputStream out_sock;
    private DataInputStream in;
    private InputStream in_sock;
    Random g;
    Game(int ile){
        g=new Random();
        this.ileGraczy=ile;
        nicks=new String[ileGraczy];
        playable=new int[ile];
        for (int i=0; i<ile; i++)
            playable[i]=1;
        activePlayer=g.nextInt(ileGraczy);
        //activePlayer=0;
    }
    @Override
    public void run(){
        //tworzenie serwera
        client=new Socket[ileGraczy];
        event=new int[ileGraczy];
        try {
        sc=new ServerSocket(PORT);
        for(int i=0; i<ileGraczy; i++){
            System.out.println("Oczekuje...");
            event[i]=-1;
            client[i]=new Socket();
            client[i] = sc.accept();
            out_sock = client[i].getOutputStream();
            out = new DataOutputStream ( out_sock );
            out.writeInt(ileGraczy);
            out.writeInt(i);
            out.writeInt(activePlayer);
            if(i==activePlayer)
                out.writeBoolean(true);
            else
                out.writeBoolean(false);
            in_sock = client[i].getInputStream();
            in = new DataInputStream ( in_sock );
            int k=in.readInt();
            nicks[i]="";
            for(int j=0; j<k;j++){
            char q=in.readChar();
            nicks[i]+=Character.toString(q);
            System.out.println(nicks[i]);
            }
        }
        for (int i=0; i<ileGraczy;i++){
            out_sock = client[i].getOutputStream();
            out = new DataOutputStream ( out_sock );
            for(int j=0;j<ileGraczy;j++){
                out.writeInt(nicks[j].length());
                out.writeChars(nicks[j]);
            }
        }
            System.out.println("Polaczono z"+ileGraczy+"graczami");
        while (running){
            Thread.sleep(1000/60);
            
            for(int i=0; i<ileGraczy;i++){
                if (playable[i]!=1)
                    continue;
                in_sock = client[i].getInputStream();
                in = new DataInputStream ( in_sock );
                event[i]=in.readInt();
                if (event[i]==0) //sprawdzenie movementu
                {
                    if (i!=activePlayer){
                        event[i]=-1;
                    }
                }
                if(event[i]==3)
                    event1=in.readInt();//delta
                if(event[i]==4){
                        event1=in.readInt();//od kogo chce kupic
                        event2=in.readInt();//jakie pole
                        event3=in.readInt();//za ile
                        event4=in.readInt();//kto chce kupic
                    }
                if(event[i]==5){
                        event1=in.readInt();//index pola
                        event2=in.readInt();//za ile
                        event3=in.readInt();//komu
                    }
            }   
            for (int i=0; i<ileGraczy; i++){
                if (playable[i]!=1)
                    continue;
                for (int j=0; j<ileGraczy; j++){   
                    if (playable[j]!=1)
                        continue;
                    out_sock = client[j].getOutputStream();
                    out = new DataOutputStream ( out_sock );
                    switch(event[i]){
                        case 0: //kostka+movement
                            movement(i);
                            break;
                        case 1: //zmiana gracza
                            if(changePlayer){
                                changePlayer=false;
                                activePlayer++;
                            if (activePlayer==ileGraczy)
                                activePlayer=0;
                            }
                            out.writeInt(event[i]);
                            out.writeInt(activePlayer);
                            break;
                        case 2: //kupno
                            out.writeInt(event[i]);
                            out.writeInt(i);
                            break;
                        case 3: //moneyChange
                            out.writeInt(event[i]);
                            out.writeInt(i);//ktory gracz
                            out.writeInt(event1);//delta
                            
                            break;
                        case 4: //oferta
                            if(j!=event1){
                                out.writeInt(-1);
                                break;
                            }
                            out.writeInt(event[i]);
                            out.writeInt(event2);//co
                            out.writeInt(event3);//za ile
                            out.writeInt(event4);//komu
                            break;
                        case 5: //sprzedaz
                            out.writeInt(event[i]);
                            out.writeInt(event1);//co
                            out.writeInt(event2);//za ile
                            out.writeInt(event3);//komu                           
                            break;
                        case 6: //wiezienie
                            out.writeInt(event[i]);
                            out.writeInt(i);
                            break;
                        case 7:
                            playable[i]=0;
                            out.writeInt(event[i]);
                            out.writeInt(i);
                            break;
                        default:
                            out.writeInt(event[i]);
                            System.out.println("Player: "+j+ event[i]+" Nie ma eventu, prosze wyjsc");
                            break;      
                        }
                } 
                kostka1=-1;
                kostka2=-1;
                event[i] =-1;
                changePlayer=true;
            }
        }
        
        } catch (Exception ex) {}
    }
    void movement(int i) throws IOException{
        if (kostka1==-1 && kostka2==-1){
            kostka1=g.nextInt(6);
            //kostka1=4;
            System.out.println("Wyslalem kostka1= "+kostka1);
            kostka2=g.nextInt(6);
            //kostka2=4;
            System.out.println("Wyslalem kostka2= "+kostka2);
        }
        out.writeInt(event[i]);
        out.writeInt(kostka1);
        out.writeInt(kostka2);
    }
}
