/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamespoly;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author mstep_000
 */
public class Pawn {
    JLabel pawns[]=new JLabel[8];
    Pawn(){
        ImageIcon obraz = new ImageIcon(getClass().getResource("Pawns/PawnYellow.png"));
        pawns[0] = new JLabel(obraz);
        
        obraz = new ImageIcon(getClass().getResource("Pawns/PawnBlue.png"));
        pawns[1] = new JLabel(obraz);
        
        obraz = new ImageIcon(getClass().getResource("Pawns/PawnGreen.png"));
        pawns[2] = new JLabel(obraz);
        
        obraz = new ImageIcon(getClass().getResource("Pawns/PawnBrown.png"));
        pawns[3] = new JLabel(obraz);
        
        obraz = new ImageIcon(getClass().getResource("Pawns/PawnCeladon.png"));
        pawns[4] = new JLabel(obraz);
        
        obraz = new ImageIcon(getClass().getResource("Pawns/PawnPink.png"));
        pawns[5] = new JLabel(obraz);
        
        obraz = new ImageIcon(getClass().getResource("Pawns/PawnRed.png"));
        pawns[6] = new JLabel(obraz);
        
        obraz = new ImageIcon(getClass().getResource("Pawns/PawnViolet.png"));
        pawns[7] = new JLabel(obraz);
    }
}
