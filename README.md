Harmonogram projektu Java - Monopoly


1. Okienka: Swing 
a)zaimplementowanie głównego okna:
-graj
-wczytaj
-wyjście
b)zaimplementowanie okna graj:
-utwórz sesje
-dołącz
-wczytaj sesje
-wróć
c)zaimplementowanie prototypu okna głównej rozgrywki:
-okno rozgrywki
-przyciski zastąpione placeholderami
-wyjście
d)Implementacja poprawnego wyświetlania(wyśrodkowanie, zablokowanie rozszerzania przez użytkownika, ustawienie rozdzielczości) , wczytanie grafik, utworzenie klasy obsługującej kliknięcia, ustawienia przycisków.
2. Zapis i odczyt plików,
-konfiguracja serwera i klienta (informacje o adresie do bazy danych)
-zapisywanie rozgrywki
-zapis/odczyt kart 
-mechanizm za pomocą którego następuje automatyczny zapis co określony czas 
3. Współbieżność: wątki, operacje atomowe, itp.
-serwer/watek który akceptuje/wysyła oraz  pracuje dla konkretnego użytkownika 
-watek klienta (kontroluje odbierane dane,może zostać przerwany w bezpieczny sposób)
-klasa komunikatów dla klienta   ?
-logika integrująca logike gry z logika serwerową
4. Bazy danych: ORM (np. Hibernate) lub JDBC dla: MySQL, PostgreSQL lub SQLite,
-baza danych ~4 tabele
-logika przechwytywania największych wygranych 
-logika autoryzacji
-logika walidacji (opcjonalnie)

5. Komunikacja sieciowa: sockety lub RMI,
-socket-serwer 
-logika klienta(taki sam interfejs jak klient)
-protokół komunikacji
-
6. Zaproponowane przez studenta (np. wzorce projektowe).
-implementacja wszystkich mechanizmów określonych w zasadach
-rzut kostka 22.03.16 zajecia 4
-ruch pionków 22.03.16 zajecia 4
mechanizm naliczania opłat
wykup/zakup/licytacja